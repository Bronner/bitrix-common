<?php

namespace Bronner\Bitrix\Common\Container;

class Container
{
    private static $instance;

    private $instances;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function addInstance(string $class, $instance)
    {
        $this->instances[$class] = $instance;
    }

    public function get(string $class)
    {
        return $this->instances[$class];
    }
}
