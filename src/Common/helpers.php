<?php

if (!function_exists('myfunc')) {
//    function myfunc($path): string
//    {
//
//    }
}

if (!function_exists('container')) {
    function container(): \Bronner\Bitrix\Common\Container\Container
    {
        return Bronner\Bitrix\Common\Container\Container::getInstance();
    }
}

if (!function_exists('asset')) {

    function asset($path): string
    {
        return container()->get(\Bronner\Bitrix\Common\Util\Assets::class)->getUrl($path);
    }
}


if (!function_exists('tpl')) {
    /**
     * Возвращает путь до файла от корня шаблона
     *
     * @param $path
     * @return string
     */
    function tpl($path)
    {
        return app()->GetTemplatePath(ltrim($path, '\\/'));
    }
}

if (!function_exists('root')) {
    /**
     * Возвращает путь до файла от корня сайта
     *
     * @param $path
     * @return string
     */
    function root($path)
    {
        return \Bitrix\Main\Application::getDocumentRoot() . '/' . ltrim($path, '\\/');
    }
}

if (!function_exists('app')) {
    /**
     * @return CMain;
     */
    function app()
    {
        global $APPLICATION;

        return $APPLICATION;
    }
}

if (!function_exists('user')) {
    /**
     * @return CUser
     */
    function user()
    {
        global $USER;

        return $USER;
    }
}

if (!function_exists('isDev')) {
    /**
     * @return bool
     */
    function isDev()
    {
        $subdomains = [
            'local.',
            'dev.',
            'test.',
        ];
        $domains = [
            'localhost',
            'local',
            'dev',
            'app'
        ];

        if (isset($_REQUEST['production'])) {
            return false;
        }

        foreach ($subdomains as $subdomain) {
            if (strpos($_SERVER['HTTP_HOST'], $subdomain) === 0) {
                return true;
            }
        }


        foreach ($domains as $domain) {
            if (end(explode('.', $_SERVER['HTTP_HOST'])) === $domain) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('plural')) {
    /**
     * Возвращает правильную форму множественного существительного в зависимости от переданного числа. <br>
     * Например: <br>
     * <code>plural(12, ['Яблоко','Яблока','Яблок'])</code> вернет форму слова "Яблок" и т.п.
     *
     * @param $number - Число
     * @param $form - Формы слов
     * @return mixed
     */
    function plural($number, $form): string
    {
        $cases = array(2, 0, 1, 1, 1, 2);

        return $form[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }
}

if (!function_exists('cleanPhoneNumber')) {
    /**
     * Удаляет из строки с номером телефона все символы, кроме чисел и знака +
     *
     * @param $phoneString
     * @return string
     */
    function cleanPhoneNumber($phoneString): string
    {
        return preg_replace('/[^+0-9]/', '', $phoneString);
    }
}

if (!function_exists('formatPrice')) {
    /**
     * Выводит форматированную стоимость
     *
     * @param $value
     * @return string
     */
    function formatPrice($value): string
    {
        return number_format($value, 0, '.', ' ');
    }
}

