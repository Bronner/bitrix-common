<?php

namespace Bronner\Bitrix\Common\Util;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;

class Assets
{
    /**
     * @var string
     */
    private $basePath;

    /**
     * @var Package
     */
    private $manifest;

    /**
     * Assets constructor.
     *
     * @param string $basePath
     * @param string $manifestPath
     */
    public function __construct(string $manifestPath, string $basePath = 'build/assets/')
    {
        $this->basePath = $basePath;
        $this->manifest = new Package(new JsonManifestVersionStrategy($manifestPath));
    }

    /**
     * Возвращает внешнюю ссылку на файл.
     *
     * @param string $file
     * @return string
     */
    public function getUrl(string $file): string
    {
        return $this->manifest->getUrl(sprintf('%s/%s',
            $this->basePath,
            $file
        ));
    }
}
